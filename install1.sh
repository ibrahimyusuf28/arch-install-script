#!/bin/sh

mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
reflector --country "United Kingdom" --sort rate --save /etc/pacman.d/mirrorlist
pacstrap /mnt base linux-firmware linux base-devel
genfstab -U /mnt > /mnt/etc/fstab
curl https://gitlab.com/ibrahimyusuf28/arch-install-script/-/raw/master/install2.sh > install2.sh
curl https://gitlab.com/ibrahimyusuf28/arch-install-script/-/raw/master/install3.sh > install3.sh
chmod +x install{2,3}.sh
cp install{2,3}.sh /mnt
arch-chroot /mnt /install2.sh
umount /dev/sda1
reboot
