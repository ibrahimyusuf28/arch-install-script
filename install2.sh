ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
pacman -S neovim
nvim /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" > /etc/locale.conf
echo "KEYMAP=uk" > /etc/vconsole.conf
echo arch > /etc/hostname
passwd
pacman -S grub
grub-install /dev/sda --target=i386-pc
grub-mkconfig -o /boot/grub/grub.cfg
pacman -S bluez{,-utils}
systemctl enable {NetworkManager,bluetooth}.service
EDITOR=nvim visudo
pacman -S zsh
useradd -G wheel,storage,power -s /bin/zsh -m ibrahim
passwd ibrahim
pacman -S xorg{,-xinit} pulseaudio{,-{alsa,bluetooth}} pulsemixer git
su ibrahim -c /install3.sh
rm /install{2,3}.sh
